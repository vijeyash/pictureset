import React, { useReducer } from "react";
import "./cardlist.css";
import Card from "../card/card";
import Button from "@mui/material/Button";

const initialState = {
  count: 0,
};

const Cardlist = (props) => {
  const reducer = (state, action) => {
    switch (action.type) {
      case "1":
        return { count: 1 };
      case "2":
        return { count: 2 };
      case "3":
        return { count: 3 };
      case "4":
        return { count: 4 };
      case "5":
        return { count: 5 };
      default:
        return initialState;
    }
  };
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <div className="card-list">
      <div className="button">
        <Button
          variant="outlined"
          color="success"
          className="cutton"
          onClick={() => {
            dispatch({ type: "1" });
          }}
        >
          Robots
        </Button>
        <Button
          variant="outlined"
          color="error"
          className="cutton"
          onClick={() => dispatch({ type: "2" })}
        >
          Monstors
        </Button>
        <Button
          variant="outlined"
          color="info"
          className="cutton"
          onClick={() => dispatch({ type: "3" })}
        >
          Bots
        </Button>
        <Button
          variant="outlined"
          color="primary"
          className="cutton"
          onClick={() => dispatch({ type: "4" })}
        >
          Cats
        </Button>
        <Button
          variant="outlined"
          color="inherit"
          className="cutton"
          onClick={() => dispatch({ type: "5" })}
        >
          Humans
        </Button>
      </div>

      {props.monstor.map((data) => (
        <Card key={data.id} monstor={data} link={state.count} />
      ))}
    </div>
  );
};

export default Cardlist;
