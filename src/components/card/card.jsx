import React from "react";
import "./card.css";

const Card = (props) => {
  return (
    <div className="card-container">
      <img
        alt="monstor"
        src={`https://robohash.org/${props.monstor.id}?set=set${props.link}&size=180x180`}
      />
      <h1>{props.monstor.name}</h1>
    </div>
  );
};

export default Card;
