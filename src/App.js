import React, { Component } from "react";
import "./App.css";
import Cardlist from "./components/cardlist/cardlist";
import TextField from "@mui/material/TextField";

class App extends Component {
  constructor() {
    super();
    this.state = {
      monstor: [],
      searchState: "",
    };
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then((data) => this.setState({ monstor: data }));
  }

  render() {
    const { monstor, searchState } = this.state;
    const filteredMonstor = monstor.filter((monstor) =>
      monstor.name.toLowerCase().includes(searchState.toLowerCase())
    );
    const theme = {
      background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    };
    return (
      <div className="App">
        <h1>Picture Sets By Vijesh</h1>
        <TextField
          label="Search"
          color="secondary"
          focused
          type="search"
          placeholder="search"
          onChange={(e) => this.setState({ searchState: e.target.value })}
        />

        <Cardlist monstor={filteredMonstor} />
      </div>
    );
  }
}

export default App;
